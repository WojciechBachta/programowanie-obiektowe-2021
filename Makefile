#
#  To sa opcje dla kompilacji
#
CXXFLAGS=-g -Iinc -Wall -pedantic -std=c++0x

__start__: obroty_3D
	./obroty_3D

obroty_3D: obj obj/main.o obj/Prostopadloscian.o obj/Macierz3x3.o obj/Wektor3D.o\
           obj/Dr3D_gnuplot_api.o obj/Dron.o obj/Graniastoslup6.o obj/InterfejsRysowania.o\
	   obj/Powierzchnia.o obj/UkladW.o
	g++ -Wall -pedantic -std=c++0x -o obroty_3D obj/main.o obj/Wektor3D.o\
                        obj/Macierz3x3.o obj/Prostopadloscian.o obj/Dr3D_gnuplot_api.o\
				obj/Dron.o obj/Graniastoslup6.o obj/InterfejsRysowania.o\
					obj/Powierzchnia.o obj/UkladW.o	-lpthread

obj:
	mkdir obj

obj/Dr3D_gnuplot_api.o: inc/Dr3D_gnuplot_api.hh src/Dr3D_gnuplot_api.cpp
	g++ -c ${CXXFLAGS} -o obj/Dr3D_gnuplot_api.o src/Dr3D_gnuplot_api.cpp

inc/Dr3D_gnuplot_api.hh: inc/Draw3D_api_interface.hh
	touch inc/Dr3D_gnuplot_api.hh

obj/main.o: src/main.cpp inc/Prostopadloscian.hh inc/Macierz3x3.hh inc/Macierz.hh\
           inc/Wektor3D.hh inc/Wektor.hh inc/Dron.hh inc/Graniastoslup6.hh inc/InterfejsRysowania.hh\
	   inc/Powierzchnia.hh inc/UkladW.hh
	g++ -c ${CXXFLAGS} -o obj/main.o src/main.cpp

obj/Prostopadloscian.o: src/Prostopadloscian.cpp inc/Prostopadloscian.hh\
         inc/Wektor3D.hh inc/Wektor.hh
	g++ -c ${CXXFLAGS} -o obj/Prostopadloscian.o src/Prostopadloscian.cpp

obj/Macierz3x3.o: src/Macierz3x3.cpp inc/Macierz3x3.hh inc/Macierz.hh inc/Wektor3D.hh inc/Wektor.hh
	g++ -c ${CXXFLAGS} -o obj/Macierz3x3.o src/Macierz3x3.cpp

obj/Wektor3D.o: src/Wektor3D.cpp inc/Wektor3D.hh inc/Wektor.hh
	g++ -c ${CXXFLAGS} -o obj/Wektor3D.o src/Wektor3D.cpp

inc/Wektor3D.hh: inc/Draw3D_api_interface.hh
	touch inc/Wektor3D.hh

obj/Dron.o: src/Dron.cpp inc/Dron.hh 
	g++ -c ${CXXFLAGS} -o obj/Dron.o src/Dron.cpp

obj/Graniastoslup6.o: src/Graniastoslup6.cpp inc/Graniastoslup6.hh\
	inc/Wektor3D.hh inc/Wektor.hh 
	g++ -c ${CXXFLAGS} -o obj/Graniastoslup6.o src/Graniastoslup6.cpp

obj/InterfejsRysowania.o: src/InterfejsRysowania.cpp inc/InterfejsRysowania.hh 
	g++ -c ${CXXFLAGS} -o obj/InterfejsRysowania.o src/InterfejsRysowania.cpp

obj/Powierzchnia.o: src/Powierzchnia.cpp inc/Powierzchnia.hh 
	g++ -c ${CXXFLAGS} -o obj/Powierzchnia.o src/Powierzchnia.cpp

obj/UkladW.o: src/UkladW.cpp inc/UkladW.hh 
	g++ -c ${CXXFLAGS} -o obj/UkladW.o src/UkladW.cpp

clean:
	rm -f obj/*.o obroty_3D
