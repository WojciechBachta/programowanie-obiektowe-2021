#ifndef DRON_HH
#define DRON_HH


#include <iostream>
#include "Prostopadloscian.hh"
#include "Graniastoslup6.hh"
#include <cmath>
#include <vector>


class Dron{
private:
    Wektor<3> srodek;
    Macierz<3> orient;
    Prostopadloscian korpus;
    std::vector<Graniastoslup6> wirniki(4);
public:
    Dron(Wektor W) : korpus(W,{2,2,1}), wirniki({{1.0,1.0,0.5},{0.5,0.5,0.5}},{{1.0,-1.0,0.5},{0.5,0.5,0.5}},{{-1.0,1.0,0.5},{0.5,0.5,0.5}},{{-1.0,-1.0,0.5},{0.5,0.5,0.5}});
    void lec(double katZ, double wysokosc, double odleglosc);
    void rysuj(drawNS::Draw3DAPI *rysownik);


}































#endif
