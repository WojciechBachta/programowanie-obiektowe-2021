#ifndef WEKTOR_HH
#define WEKTOR_HH

#include <iostream>
#include <vector>
#include <initializer_list>
#include<cmath>
#include "Dr3D_gnuplot_api.hh"


template<int ROZMIAR>
class Wektor {
private:
    std::vector<double> xy;
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
public:
    Wektor() { for(int i = 0; i < ROZMIAR; i++) this->xy.push_back(0); }
    Wektor(std::initializer_list<double> l) : xy(l){}
    Wektor<ROZMIAR> operator+(const Wektor<ROZMIAR> & arg2) const;
    Wektor<ROZMIAR> operator-(const Wektor<ROZMIAR> & arg2) const;
    Wektor<ROZMIAR> operator*(double arg2) const;
    double operator*(const Wektor<ROZMIAR> & arg2) const;
    double dlugosc();

    const double & operator[] (int ind) const;
    double & operator[] (int ind);

    drawNS::Point3D konwertuj() {return drawNS::Point3D(this->xy[0], this->xy[1], this->xy[2]);}

  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */
};


template <int ROZMIAR>
Wektor<ROZMIAR> Wektor<ROZMIAR>::operator+(const Wektor<ROZMIAR> & arg2) const
{
    Wektor<ROZMIAR> wynik;
    for(int i=0; i<ROZMIAR; i++)
        {
            wynik[i] = this->xy[i] + arg2.xy[i];
        }
    return wynik;
}

template <int ROZMIAR>
Wektor<ROZMIAR> Wektor<ROZMIAR>::operator-(const Wektor<ROZMIAR> & arg2) const
{
    Wektor<ROZMIAR> wynik;
    for(int i=0; i<ROZMIAR; i++)
        {
            wynik[i] = this->xy[i] - arg2.xy[i];
        }
    return wynik;
}

template <int ROZMIAR>
Wektor<ROZMIAR> Wektor<ROZMIAR>::operator*(double arg2) const
{
    Wektor<ROZMIAR> wynik;
    for(int i=0; i<ROZMIAR; i++)
        {
            wynik[i] = this->xy[i] * arg2;
        }
    return wynik;
}

template <int ROZMIAR>
double Wektor<ROZMIAR>::operator*(const Wektor<ROZMIAR> & arg2) const
{
    double wynik;
    for(int i=0; i<ROZMIAR; i++)
        {
            wynik += this->xy[i] * arg2.xy[i];
        }
    return wynik;
}

template <int ROZMIAR>
double Wektor<ROZMIAR>::dlugosc()
{
    double wynik = 0;
    for(int i=0; i<ROZMIAR; i++)
    {
        wynik += this->xy[i]*this->xy[i];
    }
    return sqrt(wynik);
}







/*!
 * \brief Przeciążenie dla wyjścia musi także być szablonem
 *
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt
 */
template <int ROZMIAR>
std::istream& operator >> (std::istream &Strm, Wektor<ROZMIAR> &Wek)
{
    for(int i = 0; i < ROZMIAR; i++)
    {
        Strm >> Wek[i];
    }
    return Strm;
}

/*!
 * \brief Przeciążenie dla wejścia musi także być szablonem
 *
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt
 */
template <int ROZMIAR>
std::ostream& operator << (std::ostream &Strm, const Wektor<ROZMIAR> &Wek)
{
    Strm << "[" << Wek[0];
    for(int i=1; i<ROZMIAR; i++)
    {
        Strm << ", " << Wek[i];
    }
    Strm << "]";
    return Strm;
}

template <int ROZMIAR>
const double & Wektor<ROZMIAR>::operator[] (int index) const
{
    if (index < 0 || index > ROZMIAR)
    {
        std::cerr << "indeks poza zakresem\n";
        exit(1);
    }

    return (this->xy[index]);
}

template <int ROZMIAR>
double & Wektor<ROZMIAR>::operator[] (int index)
{
    if (index < 0 || index > ROZMIAR)
    {
        std::cerr << "indeks poza zakresem\n";
        exit(1);
    }

    return (this->xy[index]);
}


#endif
