#ifndef MACIERZ_HH
#define MACIERZ_HH


#include <iostream>
#include "Wektor.hh"
#include <cmath>
#include <vector>


template<int ROZMIAR>
class Macierz{
private:
    std::vector<Wektor<ROZMIAR>> M;
    double kat;
  /*
   *  Tutaj trzeba wstawic definicje odpowiednich pol i metod prywatnych
   */
  public:
    Macierz();
    Wektor<ROZMIAR> operator* (const Wektor<ROZMIAR> & wek) const;
    const Wektor<ROZMIAR> & operator[] (int ind) const;
    Wektor<ROZMIAR> & operator[] (int ind);
    void UstawObrot(char os, double k);
    void Obroc(char os, double k){this->kat+=k; this->UstawObrot(os, kat);}
    double get_kat(){return this->kat;}

  /*
   *  Tutaj trzeba wstawic definicje odpowiednich metod publicznych
   */
};




template <int ROZMIAR>
Macierz<ROZMIAR>::Macierz()
{
    Wektor<ROZMIAR> W;
    for(int i = 0; i < ROZMIAR; i++)
    {
        this->M.push_back(W);
    }
}

template <int ROZMIAR>
Wektor<ROZMIAR> Macierz<ROZMIAR>::operator* (const Wektor<ROZMIAR> & wek) const
{
    Wektor<ROZMIAR> wynik;
    double tmp = 0;

    for(int i = 0; i < ROZMIAR; i++)
    {
        tmp = tmp + this->M[i] * wek;
        wynik[i] = tmp;
        tmp = 0;
    }

    return wynik;
}

template <int ROZMIAR>
void Macierz<ROZMIAR>::UstawObrot(char os, double k)
{
    switch(os)
    {
    case 'x':
    this->M[0][0] = 1;
    this->M[0][1] = 0;
    this->M[0][2] = 0;
    this->M[1][0] = 0;
    this->M[1][1] = cos(k);
    this->M[1][2] = -sin(k);
    this->M[2][0] = 0;
    this->M[2][1] = sin(k);
    this->M[2][2] = cos(k);
    break;
    case 'y':
    this->M[0][0] = cos(k);
    this->M[0][1] = 0;
    this->M[0][2] = sin(k);
    this->M[1][0] = 0;
    this->M[1][1] = 1;
    this->M[1][2] = 0;
    this->M[2][0] = -sin(k);
    this->M[2][1] = 0;
    this->M[2][2] = cos(k);
    break;
    case 'z':
    this->M[0][0] = cos(k);
    this->M[0][1] = -sin(k);
    this->M[0][2] = 0;
    this->M[1][0] = sin(k);
    this->M[1][1] = cos(k);
    this->M[1][2] = 0;
    this->M[2][0] = 0;
    this->M[2][1] = 0;
    this->M[2][2] = 1;
    break;
    }
    this->kat=k;
}


/*!
 * \brief Przeciążenie dla wejścia musi także być szablonem
 *
 * To przeciazenie trzeba opisac. Co ono robi. Jaki format
 * danych akceptuje. Jakie jest znaczenie parametrow itd.
 * Szczegoly dotyczace zalecen realizacji opisow mozna
 * znalezc w pliku:
 *    ~bk/edu/kpo/zalecenia.txt
 */
template <int ROZMIAR>
std::ostream& operator << (std::ostream &Strm, const Macierz<ROZMIAR> &Mac)
{
    for(int i = 0; i < ROZMIAR; i++)
    {
        Strm << Mac[i] << "\n";
    }
    return Strm;
}

template <int ROZMIAR>
const Wektor<ROZMIAR> & Macierz<ROZMIAR>::operator[] (int index) const
{
    if (index < 0 || index > ROZMIAR)
    {
        std::cerr << "indeks poza zakresem\n";
        exit(1);
    }
    return (this->M[index]);
}

template <int ROZMIAR>
Wektor<ROZMIAR> & Macierz<ROZMIAR>::operator[] (int index)
{
    if (index < 0 || index > ROZMIAR)
    {
        std::cerr << "indeks poza zakresem\n";
        exit(1);
    }
    return (this->M[index]);
}


#endif
