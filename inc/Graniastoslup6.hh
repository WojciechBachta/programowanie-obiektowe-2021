#ifndef GRANIASTOSLUP6_HH
#define GRANIASTOSLUP6_HH

#include <iostream>
#include "Macierz.hh"
#include "Wektor.hh"
#include "UkladW.hh"
#include "InterfejsRysowania.hh"
#include "Dr3D_gnuplot_api.hh"
#include <vector>



class Graniastoslup6 : public UkladW, public InterfejsRysowania {
private:
    Macierz<3> RotX;
    Macierz<3> RotY;
    Macierz<3> RotZ;
    Wektor<3> pozycja;
    Wektor<3> wymiary;
    int id_rysunku;
    std::vector<Wektor<3>> punkty;

public:
    Graniastoslup6(Wektor<3> p, Wektor<3> wym);

    void rotacja(char os, double kat);
    void translacja(Wektor<3> wek);

    const Wektor<3> & operator[] (int ind) const {return this->punkty[ind];}
    int get_id(){return id_rysunku;}


    void rysuj(drawNS::Draw3DAPI *rysownik);


};

#endif
