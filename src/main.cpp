#include <iostream>
#include <iomanip>
#include <fstream>
#include "Wektor3D.hh"
#include "Macierz3x3.hh"
#include "Prostopadloscian.hh"
#include "Dr3D_gnuplot_api.hh"
#include <cmath>

using std::vector;
using drawNS::Point3D;
using drawNS::APIGnuPlot3D;
using std::cout;
using std::endl;
using std::sin;
using std::cos;


using namespace std;

/*
 * Tu definiujemy pozostale funkcje.
 * Lepiej jednak stworzyc dodatkowy modul
 * i tam je umiescic. Ten przyklad pokazuje
 * jedynie absolutne minimum.
 */



void wait4key() {
  do {
    std::cout << "\n Press a key to continue..." << std::endl;
  } while(std::cin.get() != '\n');
}

int main()
{

    drawNS::Draw3DAPI * api = new APIGnuPlot3D(-8,8,-8,8,-8,8,1000);
    api->change_ref_time_ms(0);

    Wektor<3> W = {1,2,3};
    Wektor<3> W2 = {3,2,1};
    Macierz<3> M;

    Prostopadloscian P(W,W2);
    P.rysuj(api);

    std::cout << "Rysowanie prostokata" << std::endl;

    while(1)
    {
    std::cout << "Menu - wybierz opcje" << std::endl;
    std::cout << "o - obrot bryly o zadana sekwencje katow" << std::endl;
    std::cout << "p - przesuniecie prostokata o zadany wektor" << std::endl;
    std::cout << "w - wyswietlenie wspolrzednych wierzcholkow" << std::endl;
    std::cout << "s - sprawdzenie dlugosci przeciwleglych bokow" << std::endl;
    std::cout << "k - koniec dzialania programu" << std::endl;

    char wybor;
    std::cin >> wybor;
    switch(wybor)
    {
    case 'o':
        {
        double katX;
        double katY;
        double katZ;
        std::cout << "Podaj kat obrotu wzhledem osi x:" << std::endl;
        std::cin >> katX;
        if (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(1000, '\n');
            std::cout << "Niepoprawne dane \n";
            continue;
        }
        std::cout << "Podaj kat obrotu wzhledem osi y:" << std::endl;
        std::cin >> katY;
        if (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(1000, '\n');
            std::cout << "Niepoprawne dane \n";
            continue;
        }
        std::cout << "Podaj kat obrotu wzhledem osi z:" << std::endl;
        std::cin >> katZ;
        if (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(1000, '\n');
            std::cout << "Niepoprawne dane \n";
            continue;
        }
        double katXR = ((katX*2*3.141592653589)/360);
        double katYR = ((katY*2*3.141592653589)/360);
        double katZR = ((katZ*2*3.141592653589)/360);
        int id = P.get_id();
        P.rotacja('x', katXR);
        P.rotacja('y', katYR);
        P.rotacja('z', katZR);
        P.rysuj(api);
        api->erase_shape(id);
        break;
        }
    case 'p':
        {
        double x;
        double y;
        double z;
        std::cout << "Podaj wektor przesuniecia" << std::endl;
        std::cout << "Wektor przesuniecia (wspolrzedna x): " << std::endl;
        std::cin >> x;
        if (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(1000, '\n');
            std::cout << "Niepoprawne dane \n";
            continue;
        }
        std::cout << "Wektor przesuniecia (wspolrzedna y): " << std::endl;
        std::cin >> y;
        if (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(1000, '\n');
            std::cout << "Niepoprawne dane \n";
            continue;
        }
        std::cout << "Wektor przesuniecia (wspolrzedna z): " << std::endl;
        std::cin >> z;
        if (std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(1000, '\n');
            std::cout << "Niepoprawne dane \n";
            continue;
        }
        Wektor<3> wek = {x, y, z};
        int id = P.get_id();
        P.translacja(wek);
        P.rysuj(api);
        api->erase_shape(id);
        break;
        }
    case 'w':
        {
        std::cout << "Wspolrzedne wierzcholkow: " << std::endl;
        for(int i=0; i<8; i++)
        {
            std::cout << P[i] << std::endl;
        }
        break;
        }
    case 's':
        {
        if(((P[0]-P[1]).dlugosc()-(P[2]-P[3]).dlugosc())<0.0000001 && ((P[0]-P[1]).dlugosc()-(P[4]-P[5]).dlugosc())<0.0000001 && ((P[0]-P[1]).dlugosc()-(P[6]-P[7]).dlugosc())<0.0000001)
            std::cout << "Krotsze przeciwlegle boki sa rowne" << std::endl;
        std::cout << (P[0]-P[1]).dlugosc() << std::endl;
        std::cout << (P[2]-P[3]).dlugosc() << std::endl;
        std::cout << (P[4]-P[5]).dlugosc() << std::endl;
        std::cout << (P[6]-P[7]).dlugosc() << std::endl;
        if(((P[0]-P[3]).dlugosc()-(P[1]-P[2]).dlugosc())<0.0000001 && ((P[0]-P[3]).dlugosc()-(P[4]-P[7]).dlugosc())<0.0000001 && ((P[0]-P[3]).dlugosc()-(P[5]-P[6]).dlugosc())<0.0000001)
            std::cout << "Srednie przeciwlegle boki sa rowne" << std::endl;
        std::cout << (P[0]-P[3]).dlugosc() << std::endl;
        std::cout << (P[1]-P[2]).dlugosc() << std::endl;
        std::cout << (P[4]-P[7]).dlugosc() << std::endl;
        std::cout << (P[5]-P[6]).dlugosc() << std::endl;
        if(((P[0]-P[4]).dlugosc()-(P[1]-P[5]).dlugosc())<0.0000001 && ((P[0]-P[4]).dlugosc()-(P[2]-P[6]).dlugosc())<0.0000001 && ((P[0]-P[4]).dlugosc()-(P[3]-P[7]).dlugosc())<0.0000001)
            std::cout << "Dluzsze przeciwlegle boki sa rowne" << std::endl;
        std::cout << (P[0]-P[4]).dlugosc() << std::endl;
        std::cout << (P[1]-P[5]).dlugosc() << std::endl;
        std::cout << (P[2]-P[6]).dlugosc() << std::endl;
        std::cout << (P[3]-P[7]).dlugosc() << std::endl;
        break;
        }
    case 'k':
        {
        std::cout << "Koniec dzialania programu" << std::endl;
        return 0;
        }

    }
    }




}



























