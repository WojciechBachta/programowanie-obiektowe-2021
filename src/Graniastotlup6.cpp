#include "Graniastoslup6.hh"
#include "Macierz.hh"
#include "Wektor.hh"


Graniastoslup6::Graniastoslup6(Wektor<3> p, Wektor<3> wym)
{
    this->pozycja = p;
    this->wymiary = wym;
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    this->punkty.push_back(Wektor<3>{0.0,0.0,0.0});
    RotX.UstawObrot('x', 0);
    RotY.UstawObrot('y', 0);
    RotZ.UstawObrot('z', 0);
}



void Graniastoslup6::rotacja(char os, double kat)
{
    switch(os)
    {
    case 'x':
        this->RotX.Obroc(os, kat);
        break;
    case 'y':
        this->RotY.Obroc(os, kat);
        break;
    case 'z':
        this->RotZ.Obroc(os, kat);
        break;
    }

}

void Graniastoslup6::translacja(Wektor<3> wek)
{
    this->pozycja = this->pozycja + wek;
}

void Graniastoslup6::rysuj(drawNS::Draw3DAPI *rysownik)
    {
    this->punkty[0] = (this->RotZ*(this->RotY*(this->RotX*Wektor<3>{wymiary[0]/2, wymiary[1]/2, wymiary[2]/2})))+this->pozycja;
    this->punkty[1] = (this->RotZ*(this->RotY*(this->RotX*Wektor<3>{wymiary[0]/2, wymiary[1]/2, -wymiary[2]/2})))+this->pozycja;
    this->punkty[2] = (this->RotZ*(this->RotY*(this->RotX*Wektor<3>{wymiary[0]/2, -wymiary[1]/2, -wymiary[2]/2})))+this->pozycja;
    this->punkty[3] = (this->RotZ*(this->RotY*(this->RotX*Wektor<3>{wymiary[0]/2, -wymiary[1]/2, wymiary[2]/2})))+this->pozycja;
    this->punkty[4] = (this->RotZ*(this->RotY*(this->RotX*Wektor<3>{-wymiary[0]/2, wymiary[1]/2, wymiary[2]/2})))+this->pozycja;
    this->punkty[5] = (this->RotZ*(this->RotY*(this->RotX*Wektor<3>{-wymiary[0]/2, wymiary[1]/2, -wymiary[2]/2})))+this->pozycja;
    this->punkty[6] = (this->RotZ*(this->RotY*(this->RotX*Wektor<3>{-wymiary[0]/2, -wymiary[1]/2, -wymiary[2]/2})))+this->pozycja;
    this->punkty[7] = (this->RotZ*(this->RotY*(this->RotX*Wektor<3>{-wymiary[0]/2, -wymiary[1]/2, wymiary[2]/2})))+this->pozycja;

    id_rysunku = rysownik->draw_polyhedron(std::vector<std::vector<drawNS::Point3D>> {{
	this->punkty[0].konwertuj(), this->punkty[1].konwertuj(), this->punkty[2].konwertuj(), this->punkty[3].konwertuj()}, {
    this->punkty[4].konwertuj(), this->punkty[5].konwertuj(), this->punkty[6].konwertuj(), this->punkty[7].konwertuj()}
    });
}
